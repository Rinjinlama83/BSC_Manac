# BSC_Manc

Move Mat Item from CSI to Factory Track Demo

### Prerequisites

## CSI

### Forms <br />

BSC_Manc_ALLocation<br />
BSC_Manc_Job_PickList <br />

### IDOs <br />

BSC_MANC_ALLocation

### SQL TABLE <br />

BSC_MANC_ALLocation

### Stored Procedure

BSC_MancInsertAlLocation

## Factory Track

### Forms <br />

BSC_Manc_CSIMoveMat

### IDO <br />

BSC_Manac_ALLocationViews

### SQL Views <br />

BSC_Manac_ALLocationView

### Configure in WareHouseMobility <br />

Factory track used dynamic way to built a Menu. Please watch this videos to configure a BSC_Manc_CSIMoveMat in WareHouseMobility.

https://nainfor.webex.com/nainfor/lsr.php?RCID=0155967bb8a1494b8005cd52109d0da5

## Note CSI <br />

Please add unbound object.BSC_MANAC_Select in BSC_Manc_Job_PickList Form for Select.

## Latest Update: <br />

### FT Update

FT New Forms <br />
Update Views of Factory Track <br />
Update IDO by adding new property UM and warehouse <br />

### CSI Update <br />

Tables: Added two fields warehouse and UM <br />
IDO: Added property warehouse and UM <br />

Stored Procedure: update with two property warehouse and UM <br />
