USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_MANC_ALLocation]    Script Date: 8/29/2018 4:16:47 PM ******/
DROP TABLE [dbo].[BSC_MANC_ALLocation]
GO

/****** Object:  Table [dbo].[BSC_MANC_ALLocation]    Script Date: 8/29/2018 4:16:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_MANC_ALLocation](
	[OperNum] [dbo].[OperNumType] NOT NULL,
	[Seq] [dbo].[JobmatlSequenceType] NULL,
	[Item] [dbo].[ItemType] NULL,
	[DerStartDate] [dbo].[DateTimeType] NULL,
	[DerEndDate] [dbo].[DateTimeType] NULL,
	[JobMatsRowPointer] [uniqueidentifier] NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MANC_ALLocation_InWorkflow]  DEFAULT ((0)),
	[Location] [nvarchar](250) NULL,
	[Job] [dbo].[JobType] NULL,
	[UserName] [nvarchar](150) NULL,
	[Warehouse] [nvarchar](150) NULL,
	[UM] [dbo].[UMType] NULL,
 CONSTRAINT [PK_BSC_MANC_ALLocation] PRIMARY KEY CLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_BSC_MANC_ALLocation_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

