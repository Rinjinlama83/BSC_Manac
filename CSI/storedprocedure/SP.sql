USE [CSI_Demo_App]
GO
/****** Object:  StoredProcedure [dbo].[BSC_MancInsertAlLocation]    Script Date: 8/29/2018 4:17:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[BSC_MancInsertAlLocation]
	@Job nvarchar(150),
	@Oper nvarchar(150),
	@Seq nvarchar(150),
	@Item nvarchar(150),
	@StartDate nvarchar(150),
	@EndDate nvarchar(150),
	@JobMatsRowPointer nvarchar(200),
	@vBSC_Manc_JbrWC nvarchar(200),
	@vUserName nvarchar(200)
AS
BEGIN

	Declare @GetLocation nvarchar(150)
	Declare @GetUM nvarchar(150)


	set @GetLocation=(select loc from location_mst where wc=@vBSC_Manc_JbrWC and site_ref='Dals')
	set @GetUM=(select u_m from item_mst where item=@Item and site_ref='Dals')


	Insert into BSC_MANC_Allocation(Job,Opernum,Seq,Item,DerStartDate,DerEnddate,JobMatsRowPointer,Location,UserName,warehouse,um)

	values (@Job,@Oper,@Seq,@Item,@StartDate,@EndDate,@JobMatsRowPointer,@GetLocation,@vUserName,@vBSC_Manc_JbrWC,@GetUM)


END
